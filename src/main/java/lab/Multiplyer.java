package lab;

public class Multiplyer implements Runnable {

    private int[] row;
    private int[][] B;
    private int[] result;
    private int[][] C;
    private int k;

    public Multiplyer(int[] row, int[][] B, int[][] C, int k) {
        this.row = row;
        this.B = B;
        result = new int[B[0].length];
        this.C = C;
        this.k = k;
    }

    @Override
    public void run() {
        int tmp = 0;
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < row.length; j++) {
                tmp += row[j] * B[j][i];
            }
            result[i] = tmp;
            tmp = 0;
        }
        C[k] = result;
    }
}
