package lab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int n = ThreadLocalRandom.current().nextInt(1, 5);
        int m = ThreadLocalRandom.current().nextInt(1, 5);
        int m2 = ThreadLocalRandom.current().nextInt(1, 5);
        int[][] A = new int[n][m];
        int[][] B = new int[m][m2];
        int[][] C = new int[n][m2];

        fillMatrix(A);
        fillMatrix(B);

        List<Thread> threads = new ArrayList<>(n);

        for (int i = 0; i < n; i++) {
            Thread t = new Thread(new Multiplyer(A[i], B, C, i));
            t.start();
            threads.add(t);
        }

        for (Thread t : threads) {
            t.join();
        }

        print(A);
        print(B);
        print(C);
    }

    private static void fillMatrix(int[][] matrix) {
        Random random = new Random();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(5) + 1;
            }
        }
    }

    private static void print(int[][] matrix) {
        for (int[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }

        System.out.println("\n");
    }
}
